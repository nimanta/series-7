package sbu.cs;

import java.io.*;
import java.net.*;

public class FileServer {

    private static final int port = 5050;
    private DataInputStream in;
    private Socket socket;
    private final ServerSocket server;
    private final String directory;

    public FileServer(String directory) throws IOException {

        server = new ServerSocket(port);
        this.directory = directory;
        System.out.println("Waiting for client to connect .......");
    }

    public void startServer() throws IOException {

        socket = server.accept();
        System.out.println("Client connected to the server successfully.");

        in = new DataInputStream(socket.getInputStream());
        String fileName = in.readUTF();
        int len = in.readInt();
        byte[] byteArray = new byte[len];
        System.out.println("File " + fileName + " is being read");
        in.readFully(byteArray, 0, len);

        FileOutputStream fileOutputStream = new FileOutputStream(directory + "/" + fileName);
        fileOutputStream.write(byteArray, 0, byteArray.length);
        System.out.println("File " + fileName + " has been uploaded to " + directory + ":)");

        in.close();
        server.close();
        socket.close();
    }
}