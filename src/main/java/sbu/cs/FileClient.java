package sbu.cs;

import java.io.*;
import java.net.*;
import java.nio.file.*;

public class FileClient {

    private static final String localhost = "localhost";
    private static final int port = 5050;
    private final Socket socket;
    private final DataOutputStream out;
    private final File file;
    private final String filePath;

    public FileClient(String filePath) throws IOException {

        socket = new Socket(localhost, port);
        System.out.println("Connected.");
        out = new DataOutputStream(socket.getOutputStream());
        this.filePath = filePath;
        file = new File(filePath);
    }

    public void startClient() throws IOException {

        Path path = Paths.get(filePath);
        byte[] byteArray = Files.readAllBytes(path);

        System.out.println("Sending " + filePath + " .....");
        out.writeUTF(file.getName());
        out.writeInt(byteArray.length);
        out.write(byteArray);
        System.out.println("File has been sent completely.");

        socket.close();
        out.close();
    }
}
