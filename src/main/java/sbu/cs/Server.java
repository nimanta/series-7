package sbu.cs;

import java.io.*;

public class Server {

    public static void main(String[] args) throws IOException {

        String directory = args[0];

        FileServer server = new FileServer(directory);
        server.startServer();
    }
}
