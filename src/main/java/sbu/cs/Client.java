package sbu.cs;

import java.io.*;

public class Client {

    public static void main(String[] args) throws IOException {

        String filePath = args[0];

        FileClient client = new FileClient(filePath);
        client.startClient();
    }
}
